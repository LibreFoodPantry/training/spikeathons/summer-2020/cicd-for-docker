# Purpose
This project is an example of how to implement Gitlab's CI / CD in a project that uses Docker.

# Usefulness
CI / CD allows for code to be automatically tested and deployed whenever a change is committed. This creates a more stable and reliable master branch, and it eliminates the need to manually deploy every commit. 

# .gitlab-ci.yml
Gitlab's CI / CD pipeline is initatiated and configured by a .gitlab-ci.yml file. This file is a list of jobs that will be picked up by runners and whose behavior is defined by a set of parameters. The full reference document for this file can be found [here](https://docs.gitlab.com/ee/ci/yaml/README.html).

**Jobs** are the set of parameters to be executed by the runner. There is no limmit on how many jobs may exist within a pipeline, but each job must contain a "script" clause. A job's name is user-defined and arbitrary, but it should ideally describe the job's purpose. This project uses one job called "docker-build".

**Stages** are used to designate jobs at the same level. Jobs within the same stage will run parallel to eachother, and stages run in the order they are specified. "Build", "test", and "deploy" are the default stages when nothing else is defined. This project does not specify any stages, however the "docker-build" job is part of the "build" stage.

**Image** specifies the Docker image that the job will run on. If no image is specified, ruby:2.5 is used. This project uses docker:latest.

**Services** specifies additional Docker images. These are linked to the base image specified in the "image" clause. This project uses docker:dind.

**Default** is used to specify parameters that will be used by every job. The parameters that can be set by default are as follows: image, services, before_script, after_script, tags, cache, artifacts, retry, timeout, interruptible. A job can override a pipeline's defaults by setting the parameter it intends to override within the job. This project does not utilize this feature; a better example can be found [here](https://docs.gitlab.com/ee/ci/yaml/#global-defaults).

**Script** is a shell script executed when a job is run. Jobs require a script to be valid. This project's job, "docker-build", runs the command "docker build .".

**Before_Script & After_Script** define commands to run before and after a script respectively. Commands in before_script are run in the same shell as script, however commands in after_script are run in a seperate shell. This project does not utilize this feature; a better example can be found [here](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script)

# Docker Specific
In order to test Docker with CI / CD, the docker-in-docker service (docker:dind) must be included in the .gitlab-ci.yml file. 